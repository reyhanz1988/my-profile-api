module.exports = ({ env }) => ({
  url: env("https://smokeapps.com"),
  proxy: true,
  app: {
    keys: env.array("APP_KEYS", ["2d9SrFQFAMomFcMeVtOOOA==","G9LTWaCwMe2v13Q8gcpUXA=="]),
  },
});
